;GetStudio make file V1 2017_01_21

api = 2
core = 7.x

; For Drupal 7:
projects[] = drupal

; Modules
projects[] = views
projects[] = cck
projects[] = admin_menu
projects[] = block_class
projects[] = ckeditor_insert
projects[] = colorbox
projects[] = ctools
projects[] = disable_breadcrumbs
projects[] = entity
projects[] = eu_cookie_compliance
projects[] = exclude_node_title
projects[] = globalredirect
projects[] = google_analytics
projects[] = image_style_quality
projects[] = imce
projects[] = imce_wysiwyg
projects[] = in_field_labels
projects[] = insert
projects[] = jquery_update
projects[] = libraries
projects[] = outdatedbrowser
projects[] = path_breadcrumbs
projects[] = pathauto
projects[] = retina_images
projects[] = site_map
projects[] = skinr
projects[] = token
projects[] = views
projects[] = webform
projects[] = webform_hints
projects[] = wysiwyg

; Themes
projects[zurb_foundation] = 5.0-rc6